# -*- mode: ruby -*-
# vi: set ft=ruby :

install_basic_packages = <<-SCRIPT
  echo "Installing basic packages..."
  apt-get install curl -y

  apt-get update
  apt-get install -y software-properties-common python-software-properties

  apt-get install -y apt-transport-https ca-certificates
  apt-get update
SCRIPT

add_vagrant_keys = <<-SCRIPT
  echo "Installing vagrant keys..."
  curl -s https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub > ~/.ssh/id_rsa.pub
  curl -s https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant > ~/.ssh/id_rsa
  chmod 600 ~/.ssh/id_rsa
SCRIPT

install_jdk = <<-SCRIPT
  echo "Installing JDK..."
  
  echo "deb http://httpredir.debian.org/debian jessie-backports main" | tee -a /etc/apt/sources.list
  apt-get update
  apt-get -y upgrade
  apt-get -f -y install
  apt-get -t jessie-backports install -y openjdk-8-jdk
SCRIPT

install_sbt = <<-SCRIPT
  echo "Installing SBT..."
  echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
  apt-get update
  apt-get install -y sbt
SCRIPT

adjust_timezone = <<-SCRIPT
  echo "Adjusting TimeZone..."
  echo "Europe/Moscow" | tee /etc/timezone
  dpkg-reconfigure -f noninteractive tzdata
SCRIPT

install_mongodb = <<-SCRIPT
  echo "Installing MongoDB..."
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
  echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.2 main" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
  apt-get update
  apt-get install -y mongodb-org-server mongodb-org-shell
  mkdir /home/vagrant/db
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = "debian/jessie64"
  config.ssh.insert_key = false
  
  config.vm.define "tasktracker" do |tt|
    tt.vm.provider "virtualbox" do |vb|
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "2048"
    end
    tt.vm.network "private_network", ip: "192.168.151.50"

    tt.vm.network "forwarded_port", guest: 27017, host: 27017
    tt.vm.network "forwarded_port", guest: 9000, host: 9000

    tt.vm.provision "file", source: "./", destination: "/home/vagrant/"

    tt.vm.provision "shell", run: "once", privileged: true,  inline: install_basic_packages
    tt.vm.provision "shell", run: "once", privileged: false, inline: add_vagrant_keys
    tt.vm.provision "shell", run: "once", privileged: true,  inline: install_jdk
    tt.vm.provision "shell", run: "once", privileged: true,  inline: install_sbt
    tt.vm.provision "shell", run: "once", privileged: true,  inline: adjust_timezone
    tt.vm.provision "shell", run: "once", privileged: true,  inline: install_mongodb

    tt.vm.provision "shell", run: "always", privileged: false, inline: <<-SHELL
      echo "Starting Mongo Server and running tests..."
      sudo mongod --fork --syslog --dbpath /home/vagrant/db
      cd /home/vagrant/TaskTrackerWS
      sbt test
      #sbt run
    SHELL
  end
end
