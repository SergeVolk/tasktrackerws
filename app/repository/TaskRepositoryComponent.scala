package repository

import model._

trait TaskRepositoryComponent {

  def repository: TaskRepository

  trait TaskRepository {
    // Queries
    def findOneById(id: String): Option[Task]
    def getAll: Seq[Task]
    def getAllSortedBy(field: String, ascendingSort: Boolean): Seq[Task]

    // Commands
    def insert(task: Task): Task
    def update(task: Task): Task
    def delete(taskId: String): Option[String]
  }
}
