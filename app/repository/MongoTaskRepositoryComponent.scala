package repository

import java.util.concurrent.TimeUnit
import org.bson.conversions.Bson
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, ISODateTimeFormat}
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Sorts.{ascending, descending}
import org.mongodb.scala.model.Updates.{combine, set, unset}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase, Observable}
import scala.concurrent.Await
import scala.concurrent.duration.Duration

import infrastructure.AppConfiguration
import model._

trait MongoTaskRepositoryComponent extends TaskRepositoryComponent {

  this: AppConfiguration =>

  override def repository: TaskRepository = repo

  val tasksCollectionName: String = "tasks"

  private lazy val repo = new MongoRepository

  private class MongoRepository extends TaskRepository {

    private implicit class ImplicitObservable[C](val observable: Observable[C]) {
      def results(): Seq[C] = Await.result(observable.toFuture(), Duration(timeoutDuration, TimeUnit.SECONDS))
    }

    private val mongoClient: MongoClient = MongoClient(connectionString)
    private val database: MongoDatabase = mongoClient.getDatabase(databaseName)
    private val collection: MongoCollection[Document] = database.getCollection(tasksCollectionName)
    private val dateParser = ISODateTimeFormat.dateTimeNoMillis()
    private val dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ")

    private def task2Document(t: Task): Document = {

      val id = "_id" -> t.id
      val summary = "summary" -> t.summary
      val description = "description" -> t.description
      val priority = "priority" -> t.priority.toString
      val isContinuous = "isContinuous" -> t.isContinuous
      val creationDate = "creationDate" -> t.creationDate.toString(dateTimeFormat)

      if (t.isContinuous) {
        Document(id, summary, description, priority, isContinuous, creationDate, "dueDate" -> None)
      }
      else {
        Document(id, summary, description, priority, isContinuous, creationDate,
          {
            val dueDateOpt = t.dueDate
            if (dueDateOpt.isDefined)
              "dueDate" -> dueDateOpt.get.toString(dateTimeFormat)
            else
              "dueDate" -> None
          })
      }
    }

    private def document2Task(doc: Document): Task = {

      val id = doc.get("_id").get.asString().getValue
      val summary = doc.get("summary").get.asString().getValue
      val description = doc.get("description").get.asString().getValue
      val priority = Priority.withName(doc.get("priority").get.asString().getValue)
      val isContinuous = doc.get("isContinuous").get.asBoolean().getValue
      val creationDate = DateTime.parse(
        doc.get("creationDate").get.asString().getValue,
        dateTimeFormat
      )
      val dueDate =
        if (!isContinuous) {
          val dueDateOpt = doc.get("dueDate")
          if (dueDateOpt.isDefined)
            Some(DateTime.parse(dueDateOpt.get.asString().getValue, dateTimeFormat))
          else
            None
        }
        else {
          None
        }

      Task(id, summary, description, priority, isContinuous, creationDate, dueDate)
    }

    private def task2UpdateDocument(task: Task): Bson = {

      val pairs = combine(
        set("summary", task.summary),
        set("description", task.description),
        set("priority", task.priority.toString),
        set("isContinuous", true),
        set("creationDate", task.creationDate.toString(dateTimeFormat))
      )

      if (task.isContinuous) {
        combine(pairs,
          unset("dueDate"))
      } else {
        combine(pairs,
          {
            if (task.dueDate.isDefined)
              set("dueDate", task.dueDate.get.toString(dateTimeFormat))
            else
              unset("dueDate")
          })
      }
    }

    def insert(task: Task): Task = {
      val uuid = java.util.UUID.randomUUID.toString
      val updTask = task.copy(id = uuid)
      val doc = task2Document(updTask)
      collection.insertOne(doc).results()
      updTask
    }

    def findOneById(id: String): Option[Task] = {
      val searchResults = collection.find(equal("_id", id)).results()
      searchResults.count(p => true) match {
        case 1 => Some(document2Task(searchResults.head))
        case _ => None
      }
    }

    def getAll: Seq[Task] = collection.find().results().map(document2Task)

    def getAllSortedBy(field: String, ascendingSort: Boolean): Seq[Task] = {
      collection
        .find()
        .sort(if (ascendingSort) {
          ascending(field)
        } else {
          descending(field)
        })
        .results()
        .map(document2Task)
    }

    def update(task: Task): Task = {
      collection.updateOne(equal("_id", task.id), task2UpdateDocument(task)).results().head.getMatchedCount match {
        case 1 => task
        case _ => {
          collection.insertOne(task2Document(task)).results()
          task
        }
      }
    }

    def delete(taskId: String): Option[String] = {
      val deleteResult = collection.deleteOne(equal("_id", taskId)).results()
      deleteResult.head.getDeletedCount match {
        case 1 => Some(taskId)
        case _ => None
      }
    }
  }
}
