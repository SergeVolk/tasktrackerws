package repository

import java.util.concurrent.TimeUnit
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoDatabase, Observable}
import scala.concurrent.Await
import scala.concurrent.duration.Duration

trait MongoDbUtils
{
  this: {
    val connectionString: String
    val databaseName: String
    val timeoutDuration: Int
  } =>

  private val mongoClient: MongoClient = MongoClient(connectionString)
  private val database: MongoDatabase = mongoClient.getDatabase(databaseName)

  private def getResult[A](observable: Observable[A]): Seq[A] =
    Await.result(observable.toFuture(), Duration(timeoutDuration, TimeUnit.SECONDS))

  def dropDB(): Unit = getResult(database.drop())

  def ensureObjectExists(collectionName: String, id: String): Boolean =
    getResult(database.getCollection[Document](collectionName).find(equal("_id", id))).length == 1

  def getCollectionDocuments(collectionName: String): Seq[Document] =
    getResult(database.getCollection(collectionName).find())

  def getCollectionDocumentById(collectionName: String, id: String): Document =
    getResult(database.getCollection(collectionName).find(equal("_id", id))).head
}
