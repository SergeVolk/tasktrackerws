package model

object Priority extends Enumeration {
  val low = Value("low")
  val normal = Value("normal")
  var high = Value("high")
}