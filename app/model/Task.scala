package model

import org.joda.time.DateTime

case class Task(
                 id: String,
                 summary: String,
                 description: String,
                 priority: Priority.Value,
                 isContinuous: Boolean,
                 creationDate: DateTime,
                 dueDate: Option[DateTime]
               )