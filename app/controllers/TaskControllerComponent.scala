package controllers

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.data.validation.ValidationError
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._
import scala.util.{Failure, Success, Try}
import scala.collection.mutable.MutableList

import repository._
import model._

trait TaskControllerInterface {
  def listAllTasks(orderingField: Option[String], ascending: Option[String]): Action[AnyContent]
  def createTask: Action[AnyContent]
  def updateTask(): Action[AnyContent]
  def deleteTaskById(taskId: String): Action[AnyContent]
}

trait TaskControllerComponent extends Controller with TaskControllerInterface {

  this: TaskRepositoryComponent =>

  private val dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T00:00:00'Z")

  private implicit val priority2JsonFormat: Format[Priority.Value] = new Format[Priority.Value] {

    override def writes(priority: Priority.Value): JsValue = JsString(priority.toString)

    override def reads(json: JsValue): JsResult[Priority.Value] = {
      if (Priority.values.map(_.toString).contains(json.as[String])) {
        JsSuccess(Priority.withName(json.as[String]))
      }
      else {
        JsError(ValidationError(s"Unknown priority value '${json.as[String]}'."))
      }
    }
  }

  // Use own DateTime formatter
  private implicit val ISO8601DateJsonFormat: Format[DateTime] = new Format[DateTime] {

    override def writes(date: DateTime): JsValue = JsString(date.toString(dateTimeFormat))

    override def reads(json: JsValue): JsResult[DateTime] = {
      Try({
        DateTime.parse(json.as[String], dateTimeFormat)
      }) match {
        case Success(parsedDateTime) => JsSuccess(parsedDateTime)
        case Failure(e) => JsError(ValidationError(s"Cannot parse the date, expected format yyyy-MM-ddTHH:mm:ssZ. Exception $e."))
      }
    }
  }

  private implicit val taskReader: Reads[Task] = //Json.reads[Task]
    (
      (JsPath \ "id").read[String] and
        (JsPath \ "summary").read[String] and
        (JsPath \ "description").read[String] and
        (JsPath \ "priority").read[Priority.Value] and
        (JsPath \ "isContinuous").read[Boolean] and
        (JsPath \ "creationDate").read[DateTime] and
        (JsPath \ "dueDate").readNullable[DateTime]
      ) (Task.apply _)

  private implicit val taskWriter: Writes[Task] = //Json.writes[Task]
    (
      (JsPath \ "id").write[String] and
        (JsPath \ "summary").write[String] and
        (JsPath \ "description").write[String] and
        (JsPath \ "priority").write[Priority.Value] and
        (JsPath \ "isContinuous").write[Boolean] and
        (JsPath \ "creationDate").write[DateTime] and
        (JsPath \ "dueDate").writeNullable[DateTime]
      ) (unlift(Task.unapply))

  private implicit val validationErrorWriter: Writes[ValidationErrorDescription] = Json.writes[ValidationErrorDescription]

  private def taskNotFoundStatus = NotFound("No task with such Id.")

  private def processRepositoryAction(jsonBody: Option[JsValue], action: Task => Task, success: JsValue => Result): Result = {
    jsonBody match {
      case Some(jsonBlock) =>
        Json.fromJson[Task](jsonBlock) match {
          case JsSuccess(task, _) =>
            customTaskValidation(task) match {
              case None => success(Json.toJson(action(task)))
              case Some(errors) => NotAcceptable(Json.toJson(errors))
            }

          case JsError(errors) => NotAcceptable(Json.toJson(errors.flatMap { e => e._2.map(ve => ValidationErrorDescription(e._1.toJsonString, ve.message)) }))
        }
      case None => NotAcceptable
    }
  }

  private def customTaskValidation(task: Task): Option[Seq[ValidationErrorDescription]] = {

    val errors = MutableList[ValidationErrorDescription]()

    if (task.summary.isEmpty) {
      errors += ValidationErrorDescription("summary", "summary must always be present.")
    }

    if (task.isContinuous && task.dueDate.isDefined) {
      errors += ValidationErrorDescription("dueDate", "DueDate must not be present for endless tasks.")
    }

    if (errors.nonEmpty) {
      Some(errors)
    }
    else {
      None
    }
  }

  private case class ValidationErrorDescription(path: String, errorDescription: String)

  def getTaskById(id: String): Action[AnyContent] = Action {
    repository.findOneById(id) match {
      case Some(task) => Ok(Json.toJson(task))
      case None => taskNotFoundStatus
    }
  }

  def listAllTasks(orderingField: Option[String], ascending: Option[String]): Action[AnyContent] = Action {
    val tasks =
      orderingField match {
        case None => repository.getAll
        case Some(field) => {
          val ascendingStr = ascending.getOrElse("false").trim.toLowerCase
          val isAscendingSort = Try(ascendingStr.toBoolean).getOrElse(false)
          repository.getAllSortedBy(field, ascendingSort = isAscendingSort)
        }
      }
    Ok(Json.toJson(tasks))
  }

  def createTask: Action[AnyContent] = Action {
    request => processRepositoryAction(request.body.asJson, repository.insert, v => Created(v))
  }

  def updateTask(): Action[AnyContent] = Action {
    request => processRepositoryAction(request.body.asJson, repository.update, v => Ok(v))
  }

  def deleteTaskById(taskId: String): Action[AnyContent] = Action {
    repository.delete(taskId) match {
      case Some(deletedTaskId) => Ok(deletedTaskId)
      case None => taskNotFoundStatus
    }
  }
}
