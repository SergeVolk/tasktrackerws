package infrastructure

import com.typesafe.config.ConfigFactory

trait AppConfiguration {
  val connectionString: String
  val databaseName: String
  val timeoutDuration: Int = 10
}

trait ProductionAppConfiguration extends AppConfiguration {
  private val config = ConfigFactory.load

  val connectionString: String = config.getString("database.connectionString")
  val databaseName: String = config.getString("database.name")
  override val timeoutDuration: Int = 5
}
