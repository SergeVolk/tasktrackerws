import org.joda.time.DateTime
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec

import model._
import repository.{MongoDbUtils, MongoTaskRepositoryComponent}

class RepositorySpec extends PlaySpec with BeforeAndAfter {

  private val testCompletableTaskDo1 = Task("", "Do 1", "Do 1.1 ... 1.N", Priority.high, false, new DateTime("2018-01-01"), Some(new DateTime("2019-01-01")))
  private val testEndlessTaskDo2 = Task("", "Do 2", "Do 2 every morning until death", Priority.normal, true, new DateTime("2018-01-01"), None)

  private object mongoUtils extends TestAppConfiguration with MongoDbUtils

  before {
    mongoUtils.dropDB()
  }

  "TaskRepository" should {

    object repoComp extends MongoTaskRepositoryComponent with TestAppConfiguration

    "create a document on insertion" in {
      val insertedTask = repoComp.repository.insert(testCompletableTaskDo1)
      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, insertedTask.id) mustBe true
    }

    "return existing document or none" in {
      val insertedTask = repoComp.repository.insert(testCompletableTaskDo1)
      val documentFromDatabase = repoComp.repository.findOneById(insertedTask.id)
      documentFromDatabase mustBe Some(testCompletableTaskDo1.copy(id = insertedTask.id))

      repoComp.repository.findOneById("fake id") mustBe None
    }

    "list all created tasks" in {
      repoComp.repository.getAll mustBe Seq[Task]()
      repoComp.repository.insert(testCompletableTaskDo1)
      repoComp.repository.insert(testCompletableTaskDo1.copy(summary = "Do 3"))

      val tasksAfterInsertions = repoComp.repository.getAll
      tasksAfterInsertions.map(t => t.summary).sortBy(s => s) mustBe Seq[String]("Do 1", "Do 3")
    }

    "update existing task" in {
      val insertedTaskId = repoComp.repository.insert(testCompletableTaskDo1).id
      val updatedTask = testCompletableTaskDo1.copy(
        id = insertedTaskId,
        summary = "Do 3",
        description = "Do 3.1, 3.2",
        priority = Priority.low,
        isContinuous = true,
        creationDate = new DateTime("2028-01-01"),
        None)
      val updateResult = repoComp.repository.update(updatedTask)
      updateResult mustBe updatedTask
      val collectionDocuments = mongoUtils.getCollectionDocuments(repoComp.tasksCollectionName)

      collectionDocuments.length mustBe 1
      collectionDocuments.head.get("summary").get.asString().getValue mustBe "Do 3"
      collectionDocuments.head.get("description").get.asString().getValue mustBe "Do 3.1, 3.2"
      collectionDocuments.head.get("priority").get.asString().getValue mustBe "low"
      collectionDocuments.head.get("isContinuous").get.asBoolean().getValue mustBe true
      collectionDocuments.head.get("creationDate").get.asString().getValue mustBe "2028-01-01T00:00:00+0300"
    }

    "update of non-existent task should create one" in {
      val updateResult = repoComp.repository.update(testCompletableTaskDo1)
      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, testCompletableTaskDo1.id) mustBe true
    }

    "delete existing task" in {
      val insertedTaskId = repoComp.repository.insert(testCompletableTaskDo1).id
      val deleteResult = repoComp.repository.delete(insertedTaskId)
      deleteResult mustBe Some(insertedTaskId)
      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, insertedTaskId) mustBe false

      repoComp.repository.delete("fakeId") mustBe None
    }

    "create and delete infinite task" in {
      val insertedTaskId = repoComp.repository.insert(testEndlessTaskDo2).id
      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, insertedTaskId) mustBe true
      val insertedDocument = mongoUtils.getCollectionDocumentById(repoComp.tasksCollectionName, insertedTaskId)

      insertedDocument.get("summary").get.asString().getValue mustBe "Do 2"
      insertedDocument.get("creationDate").get.asString().getValue mustBe "2018-01-01T00:00:00+0300"

      repoComp.repository.delete(insertedTaskId)

      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, insertedTaskId) mustBe false

    }

    "update finite task to infinite and back" in {
      val insertedTaskId = repoComp.repository.insert(testCompletableTaskDo1).id
      mongoUtils.ensureObjectExists(repoComp.tasksCollectionName, insertedTaskId) mustBe true

      repoComp.repository.update(testCompletableTaskDo1.copy(id = insertedTaskId, isContinuous = true))

      val updatedDocument = mongoUtils.getCollectionDocumentById(repoComp.tasksCollectionName, insertedTaskId)

      updatedDocument.get("summary").get.asString().getValue mustBe "Do 1"
      updatedDocument.get("dueDate") mustBe None

      repoComp.repository.update(testEndlessTaskDo2.copy(id = insertedTaskId)).id

      val updatedNewDocument = mongoUtils.getCollectionDocumentById(repoComp.tasksCollectionName, insertedTaskId)

      updatedNewDocument.get("summary").get.asString().getValue mustBe "Do 2"
      updatedNewDocument.get("dueDate") mustBe None
    }
  }
}
