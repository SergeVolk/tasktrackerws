import org.joda.time.DateTime
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play._
import play.api.test.Helpers._
import play.api.test._
import controllers.TaskControllerComponent
import repository.TaskRepositoryComponent
import model._

class ControllerSpec extends PlaySpec with OneAppPerTest with MockFactory {

  "TaskController" should {

    val testTask1 = Task("1", "Do 1", "Do 1.1 ... 1.N", Priority.normal, false, new DateTime("2018-01-01"), None)
    val testTask1Json = """{"id":"1","summary":"Do 1","description":"Do 1.1 ... 1.N","priority":"normal","isContinuous":false,""" +
      """"creationDate":"2018-01-01T00:00:00+0300"}"""

    val testTask1WithEmptyId = testTask1.copy(id = "")
    val testTask1WithEmptyIdJson = """{"id":"","summary":"Do 1","description":"Do 1.1 ... 1.N","priority":"normal","isContinuous":false,""" +
      """"creationDate":"2018-01-01T00:00:00+0300"}"""

    val testTask2 = Task("2", "Do 2", "Do 2.1 ... 2.N", Priority.high, false, new DateTime("2019-01-01"), Some(new DateTime("2020-01-01")))
    val testTask2Json = """{"id":"2","summary":"Do 2","description":"Do 2.1 ... 2.N","priority":"high","isContinuous":false,""" +
      """"creationDate":"2019-01-01T00:00:00+0300","dueDate":"2020-01-01T00:00:00+0300"}"""

    val testEndlessTask3 = Task("3", "Do 3", "Do 3...", Priority.low, true, new DateTime("2018-01-01"), None)
    val testEndlessTask3Json = """{"id":"3","summary":"Do 3","description":"Do 3...","priority":"low","isContinuous":true,""" +
      """"creationDate":"2018-01-01T00:00:00+0300"}"""

    val testEndlessTask3WithEmptyId = testEndlessTask3.copy(id = "")
    val testEndlessTask3WithEmptyIdJson = """{"id":"","summary":"Do 3","description":"Do 3...","priority":"low","isContinuous":true,""" +
      """"creationDate":"2018-01-01T00:00:00+0300"}"""

    "return correct json result based on object provided by the Task Controller" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {

        private val repoMock = mock[TaskRepository]
        (repoMock.findOneById _).expects("1").returning(Some(testTask1)).once
        (repoMock.findOneById _).expects("2").returning(None).once
        (repoMock.insert _).expects(testTask1WithEmptyId).returning(testTask1).anyNumberOfTimes
        override val repository: TaskRepository = repoMock
      }

      val resultForNonExistingId = call(controller.getTaskById("2"), FakeRequest())
      contentAsString(resultForNonExistingId) mustBe "No task with such Id."
      status(resultForNonExistingId) mustBe NOT_FOUND

      val resultForExistingId = call(controller.getTaskById("1"), FakeRequest())
      status(resultForExistingId) mustBe OK
      contentAsString(resultForExistingId) mustBe testTask1Json
    }

    "create a task on a valid request" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {

        private val repoMock = mock[TaskRepository]
        (repoMock.insert _).expects(testTask1WithEmptyId).returning(testTask1).anyNumberOfTimes
        override val repository: TaskRepository = repoMock
      }

      val creationCallResult = call(controller.createTask,
        FakeRequest(
          Helpers.POST,
          controllers.routes.TaskController.createTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testTask1WithEmptyIdJson))
        status(creationCallResult) mustBe CREATED
        contentAsString(creationCallResult) mustBe testTask1Json
    }

    "return all tasks" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.getAll _).expects().returning(Seq(testTask1, testTask2))
        override val repository: TaskRepository = repoMock
      }

      val listingCallResult = call(controller.listAllTasks(None, None), FakeRequest())

      status(listingCallResult) mustBe OK
      contentAsString(listingCallResult) mustBe s"[$testTask1Json,$testTask2Json]"
    }

    "update task" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.update _).expects(testTask1).returning(testTask1).once
        override val repository: TaskRepository = repoMock
      }

      val updateCallResult = call(controller.updateTask(),
        FakeRequest(
          Helpers.PUT,
          controllers.routes.TaskController.updateTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testTask1Json))

      status(updateCallResult) mustBe OK
      contentAsString(updateCallResult) mustBe testTask1Json
    }

    "delete task" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.delete _).expects("1").returning(Some("1")).once
        (repoMock.delete _).expects("2").returning(None).once
        override val repository: TaskRepository = repoMock
      }

      val deleteCallResult = call(controller.deleteTaskById("1"),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.TaskController.deleteTaskById("1").url))

      status(deleteCallResult) mustBe OK
      contentAsString(deleteCallResult) mustBe "1"

      val failedUpdateCallResult = call(controller.deleteTaskById("2"),
        FakeRequest(
          Helpers.DELETE,
          controllers.routes.TaskController.deleteTaskById("2").url))

      status(failedUpdateCallResult) mustBe NOT_FOUND
      contentAsString(failedUpdateCallResult) mustBe """No task with such Id."""
    }

    "create an endless task on a valid request" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.insert _).expects(testEndlessTask3WithEmptyId).returning(testEndlessTask3).anyNumberOfTimes
        override val repository: TaskRepository = repoMock
      }

      val creationCallResult = call(controller.createTask,
        FakeRequest(
          Helpers.POST,
          controllers.routes.TaskController.createTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testEndlessTask3WithEmptyIdJson))
      status(creationCallResult) mustBe CREATED
      contentAsString(creationCallResult) mustBe testEndlessTask3Json
    }

    "update endless task" in {
    
      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.update _).expects(testEndlessTask3).returning(testEndlessTask3).once
        override val repository: TaskRepository = repoMock
      }

      val updateCallResult = call(controller.updateTask(),
        FakeRequest(
          Helpers.PUT,
          controllers.routes.TaskController.updateTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          testEndlessTask3Json))

      status(updateCallResult) mustBe OK
      contentAsString(updateCallResult) mustBe testEndlessTask3Json
    }

    "list both finite and infinite tasks" in {
    
      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        private val repoMock = mock[TaskRepository]
        (repoMock.getAll _).expects().returning(Seq(testTask1, testEndlessTask3))
        override val repository: TaskRepository = repoMock
      }

      val listingCallResult = call(controller.listAllTasks(None, None), FakeRequest())

      status(listingCallResult) mustBe OK
      contentAsString(listingCallResult) mustBe s"[$testTask1Json,$testEndlessTask3Json]"
    }

    "send 404 on a bad request" in {
      route(app, FakeRequest(GET, "/badReq")).map(status) mustBe Some(NOT_FOUND)
    }

    "fail with HTTP NotAcceptable or nonparseable or empty post and provide validation errors" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        override val repository: TaskRepository = mock[TaskRepository]
      }

      status(call(controller.createTask, FakeRequest())) mustBe NOT_ACCEPTABLE
      val invalidRequest = call(controller.createTask,
        FakeRequest(
          Helpers.POST,
          controllers.routes.TaskController.createTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          """ {"notId": "_"} """))

      status(invalidRequest) mustBe NOT_ACCEPTABLE
      contentAsString(invalidRequest) mustBe
        """[{"path":"obj.priority","errorDescription":"error.path.missing"},""" +
          """{"path":"obj.creationDate","errorDescription":"error.path.missing"},""" +
          """{"path":"obj.id","errorDescription":"error.path.missing"},""" +
          """{"path":"obj.summary","errorDescription":"error.path.missing"},""" +
          """{"path":"obj.description","errorDescription":"error.path.missing"},""" +
          """{"path":"obj.isContinuous","errorDescription":"error.path.missing"}]"""
    }

    "validate an task having broken fields" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        override val repository: TaskRepository = mock[TaskRepository]
      }

      val invalidTaskJsonDateFormatBroken = """{"id":"","summary":"Do X","description":"...","priority":"normal",""" +
        """"isContinuous":false,"creationDate":"2020-12-12"}"""

      val creationCallResult = call(controller.createTask,
        FakeRequest(
          Helpers.POST,
          controllers.routes.TaskController.createTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidTaskJsonDateFormatBroken))
      status(creationCallResult) mustBe NOT_ACCEPTABLE
      contentAsString(creationCallResult) mustBe
        """[{"path":"obj.creationDate","errorDescription":"Cannot""" +
        """ parse the date, expected format yyyy-MM-ddTHH:mm:ssZ. Exception java.lang.IllegalArgumentException:""" +
        """ Invalid format: \"2020-12-12\" is too short."}]"""
    }

    "validate a task with unknown priority" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {
        override val repository: TaskRepository = mock[TaskRepository]
      }

      val invalidTaskUnknownPriority = """{"id":"","summary":"Do X","description":"...","priority":"urgent",""" +
        """"isContinuous":false,"creationDate":"2018-01-01T00:00:00+0300"}"""

      val creationCallResult = call(controller.createTask,
        FakeRequest(
          Helpers.POST,
          controllers.routes.TaskController.createTask().url,
          FakeHeaders(Seq("Content-type" -> "application/json")),
          invalidTaskUnknownPriority))
      status(creationCallResult) mustBe NOT_ACCEPTABLE
      contentAsString(creationCallResult) mustBe """[{"path":"obj.priority","errorDescription":"Unknown""" +
        """ priority value 'urgent'."}]"""
    }

    "list in ordered fashion" in {

      object controller extends TaskControllerComponent with TaskRepositoryComponent with TestAppConfiguration {

        private val repoMock = mock[TaskRepository]
        (repoMock.getAllSortedBy _).expects(where {
          (field: String, ascendingSort: Boolean) => field == "summary" && ascendingSort
        }).returning(Seq(testTask1, testEndlessTask3)).anyNumberOfTimes()
        (repoMock.getAllSortedBy _).expects(where {
          (field: String, ascendingSort: Boolean) => field == "summary" && !ascendingSort
        }).returning(Seq(testEndlessTask3, testTask1)).anyNumberOfTimes()
        override val repository: TaskRepository = repoMock
      }

      val listingCallResult1 = call(controller.listAllTasks(Some("summary"), None), FakeRequest())
      contentAsString(listingCallResult1) mustBe s"[$testEndlessTask3Json,$testTask1Json]"

      val listingCallResult2 = call(controller.listAllTasks(Some("summary"), Some("true")), FakeRequest())
      contentAsString(listingCallResult2) mustBe s"[$testTask1Json,$testEndlessTask3Json]"

      val listingCallResult3 = call(controller.listAllTasks(Some("summary"), Some("false")), FakeRequest())
      contentAsString(listingCallResult3) mustBe s"[$testEndlessTask3Json,$testTask1Json]"
    }
  }
}
