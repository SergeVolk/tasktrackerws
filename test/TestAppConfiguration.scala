import infrastructure.AppConfiguration

trait TestAppConfiguration extends AppConfiguration  {
  val connectionString: String = "mongodb://localhost"
  val databaseName: String = "TaskTracker_tests"
}
