# Task Tracking API

## About

This software is a simple RESTful web-service that allows users to create, view, modify and delete tasks.
"Task" is a kind of "To-Do" containing information about what/when something should be done and how important it is.

## Some technical notes

This web-service:

- is written in Scala;
- uses Play! API for request/response management;
- uses MongoDB for storing created tasks;
- is tested using ScalaTest+ engine

## Installation & Running

### Local

- Prerequisites 

    - JDK 1.7+
    - SBT
    - Unoccupied port 9000
    - MongoDB 2.6+

- Launching the API
    
    - Provide an existing database on MongoDB server 
    - Update conf/application.conf to point to the correct MongoDB instance
    - It is pointing to the TaskTracker database on local instance of MongoDB
    - sbt run
    - point your rest client onto http://localhost:9000/

- Running the tests

    - Provide an existing database on MongoDB server
    - Update connection strings in TestAppConfiguration tests/RepositorySpec.scala to point to the correct MongoDB instance if needed
        - It is pointing to the TaskTracker (a version for tests) database on local instance of MongoDB
    - sbt test

### Inside VM

- Prerequisites

    - Oracle virtualbox 5.0+
    - Vagrant
        - Compatible with your virtualbox version
    - Rsync
    - SSH
    - Port 9000 free

- Running the API

    - vagrant up
    - point your rest client onto http://localhost:9000/