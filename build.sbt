name := "TaskTrackerWS"

version := "0.1"

lazy val tasktrackerws = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.mongodb.scala" %% "mongo-scala-driver" % "1.1.1",
  "com.github.nscala-time" %% "nscala-time" % "2.12.0",
  "org.scalatestplus" % "play_2.11" % "1.4.0" % Test,
  "org.mockito" % "mockito-core" % "1.9.5" % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % Test
)
